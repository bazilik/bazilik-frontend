import Vue from 'vue'
import Router from 'vue-router'
import store from './store'

import Menu from './components/menu/Menu'
import StockList from './components/stock/StockList'
import Auth from './components/auth/Auth'
import NotFound from './components/common/NotFound'
import Index from './components/common/Index'
import Register from './components/auth/Register'
import Profile from './components/auth/Profile'
import OrderList from './components/order/OrderList'
import AuthManager from "./components/auth/auth";
import CommentList from './components/comments/CommentList'

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      component: Auth,
      meta: {
        title: "Вход",
        requiresAuth: false
      }
    },
    {
      path: "/logout",
      meta: {
        requiresAuth: true
      },
      beforeEnter(to, from, next) {
        AuthManager.signOut().then(() => { router.replace('login') });
      }
    },
    {
      path: '/register',
      component: Register,
      meta: {
        title: "Регистрация",
        requiresAuth: false
      }
    },
    {
      path: '/profile',
      component: Profile,
      meta: {
        title: "Профиль",
        requiresAuth: true
      }
    },
    {
      path: '/menu',
      component: Menu,
      meta: {
        title: "Меню",
        requiresAuth: true
      }
    },
    {
      path: '/stocks',
      component: StockList,
      meta: {
        title: "Акции",
        requiresAuth: true
      }
    },
    {
      path: '/orders',
      component: OrderList,
      meta: {
        title: "Заказы",
        requiresAuth: true
      }
    },
    {
      path: '/comments',
      component: CommentList,
      meta: {
        title: "Отзывы",
        requiresAuth: true
      }
    },
    {
      path: '/',
      meta: {
        title: "Базилик",
        requiresAuth: false
      },
      component: Index
    },
    {
      path: '*',
      meta: {
        title: "404 Not Found",
        requiresAuth: false
      },
      component: NotFound
    }
  ]
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;

  let isAuth = store.getters.isAuth;
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !isAuth) next("login");
  // else if (!requiresAuth && isAuth) next("menu");
  else next();
});

export default router;
