
const state = {
  phoneNumber: null,
  firebaseToken: null,
  apiToken: null,
  user: null
};

const getters = {
  isAuth: state => state.user != null,
  phoneNumber: state => state.phoneNumber,
  firebaseToken: state => state.firebaseToken,
  apiToken: state => state.apiToken,
  user: state => state.user
};

const mutations = {
  initUserData(state, userData) {
    state.phoneNumber = userData.phoneNumber;
    state.firebaseToken = userData.firebaseToken;
    state.apiToken = userData.apiToken;
    state.user = userData.user;
  },
  resetUserData(state) {
    state.phoneNumber = null;
    state.firebaseToken = null;
    state.apiToken = null;
    state.user = null;
  }
};

const actions = {

};

export default {
  state,
  getters,
  mutations,
  actions
}
