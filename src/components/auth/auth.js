import firebase from 'firebase'
import {api} from "../../main";

export default class AuthManager {

  config = {
    apiKey: "AIzaSyA1Hvgov9OorOUgnbdY-v5dozPYp41R6VE",
    authDomain: "bazilik-2f202.firebaseapp.com",
    databaseURL: "https://bazilik-2f202.firebaseio.com",
    projectId: "bazilik-2f202",
    storageBucket: "bazilik-2f202.appspot.com",
    messagingSenderId: "626951163576"
  };

  userData = {
    phoneNumber: null,
    firebaseToken: null,
    apiToken: null,
    user: null
  };

  onSignedIn = new Event("onSignedIn");
  onSignedOut = new Event("onSignedOut");
  onUserNotExists = new Event("onUserNotExists");

  constructor() {
    this.signIn();
  }

  signIn() {
    firebase.initializeApp(this.config);
    firebase.auth().onAuthStateChanged((user) => {
      if (!user) {
        this.userData = null;
        dispatchEvent(this.onSignedOut);
        return;
      }

      this.userData.phoneNumber = user.phoneNumber;
      this.getFirebaseToken(user)
        .then(() => this.getApiToken())
        .then(() => this.checkUserExists());

    });
  }

  static signOut() {
    if (!firebase.auth().currentUser) return;
    return firebase.auth().signOut();
  }

  getFirebaseToken(user) {
    return user.getIdToken().then(firebaseToken => {
      this.userData.firebaseToken = firebaseToken;
    });
  }

  getApiToken() {
    return api.get("auth/api").then(response => {
      this.userData.apiToken = response.data.token;
    });
  }

  checkUserExists() {
    return api.get("auth/check", {phone: this.userData.phoneNumber}).then(response => {
      const apiUserExists = response.data;
      if (!apiUserExists.exists) {
        dispatchEvent(this.onUserNotExists);
        return;
      }

      this.userData.user = apiUserExists.user;
      dispatchEvent(this.onSignedIn);
    });
  }

}
