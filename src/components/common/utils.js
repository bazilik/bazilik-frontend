
export default class Utils {
  static toDateTimeString(unix) {
    let date = new Date(unix);
    return date.getDate() + '.' + (date.getMonth() + 1).toString().padStart(2, '0') + ' '
      + date.getHours() + ':' + date.getMinutes().toString().padStart(2, '0');
  }
}

