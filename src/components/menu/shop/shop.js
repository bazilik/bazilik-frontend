import {api} from "../../../main";
import store from "../../../store";

const state = {
  shopList: [],
  currentShop: null
};

const getters = {
  shopList: state => state.shopList,
  currentShop: state => state.currentShop,
  currentShopName: state => state.currentShop == null ? "" : state.currentShop.name
};

const mutations = {
  setShopList(state, list) {
    state.shopList = list;
  },
  setCurrentShop(state, shop) {
    state.currentShop = shop;
    store.dispatch("getCategoryList");
  }
};

const actions = {
  addShop({commit, dispatch}, data) {
    let shop = {
      name: data.name
    };

    let formData = new FormData();
    formData.append('shop', JSON.stringify(shop));
    formData.append('logo', data.logo);

    api.post('shop', formData).then(category => {
      dispatch("getShopList");
    });
  },
  async getShopList({commit}) {
    await api.get('shop/owner', {userId: store.getters.user.id}).then(response => {
      commit("setShopList", response.data);
      if (store.getters.currentShop == null) {
        commit("setCurrentShop", store.getters.shopList[0]);
      }
    })
  }
};

export default {
  state,
  getters,
  mutations,
  actions
}

