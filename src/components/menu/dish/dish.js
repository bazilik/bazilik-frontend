import store from "../../../store";
import {api} from "../../../main";

const state = {
  dishList: []
};

const getters = {
  dishList: state => state.dishList
};

const mutations = {
  setDishList(state, list) {
    state.dishList = list
  },
};

const actions = {
  addDish({commit, dispatch}, data) {
    let dish = {
      name: data.name,
      description: data.description,
      price: data.price
    };

    let formData = new FormData();
    formData.append('dish', JSON.stringify(dish));
    formData.append('photo', data.photo);
    formData.append('categoryId', JSON.stringify(store.getters.currentCategory.id));

    api.post('dish', formData).then(() => {
      dispatch("getDishList").then(() => {})
    });
  },
  getDishList({commit}) {
    state.dishList = [];
    let params = {
      categoryId: store.getters.currentCategory.id
    };
    api.get('dish', params).then(response => {
      let dishList = response.data;
      commit("setDishList", dishList);
    })
  }
};

export default {
  state,
  getters,
  mutations,
  actions
}
