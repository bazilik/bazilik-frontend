import store from "../../../store";
import {api} from "../../../main";

const state = {
  categoryList: [],
  currentCategory: null
};

const getters = {
  categoryList: state => state.categoryList,
  currentCategory: state => state.currentCategory
};

const mutations = {
  setCategoryList(state, list) {
    state.categoryList = list
  },
  setCurrentCategory(state, category) {
    state.currentCategory = category;
    store.dispatch("getDishList");
  }
};

const actions = {
  addCategory({commit, dispatch}, data) {
    let category = {
      name: data.name
    };

    let formData = new FormData();
    formData.append('category', JSON.stringify(category));
    formData.append('photo', data.photo);
    formData.append('shopId', JSON.stringify(store.getters.currentShop.id));

    api.post('category', formData).then(() => {
      dispatch("getCategoryList").then(() => {})
    });
  },
  getCategoryList({commit}) {
    state.categoryList = [];
    let params = {
      shopId: store.getters.currentShop.id
    };
    api.get('category', params).then(response => {
      let categoryList = response.data;
      commit("setCategoryList", categoryList);
      commit("setCurrentCategory", categoryList[0]);
    })
  }
};

export default {
  state,
  getters,
  mutations,
  actions
}
