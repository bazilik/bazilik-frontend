import store from "../../store";
import {api} from "../../main";
import Utils from "../common/utils";

const state = {
  stockList: []
};

const getters = {
  stockList: state => state.stockList.map(function (stock) {
    stock.expiredAt = Utils.toDateTimeString(stock.expiredAt);
    return stock;
  })
};

const mutations = {
  setStockList(state, list) {
    state.stockList = list
  }
};

const actions = {
  addStock({commit, dispatch}, data) {
    let stock = {
      description: data.description,
      expiredAt: data.expiredAt
    };

    let formData = new FormData();
    formData.append('stock', JSON.stringify(stock));
    formData.append('photo', data.photo);
    formData.append('shopId', JSON.stringify(store.getters.currentShop.id));

    api.post('stock', formData).then(() => {
      dispatch("getStockList").then(() => {})
    });
  },
  getStockList({commit, dispatch}) {
    let params = {
      shopId: store.getters.currentShop.id
    };
    api.get('stock', params).then(response => {
      let stockList = response.data;
      commit("setStockList", stockList);
    })
  }
};

export default {
  state,
  getters,
  mutations,
  actions
}
