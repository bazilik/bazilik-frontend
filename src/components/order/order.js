import store from "../../store";
import {api} from "../../main";
import Utils from "../common/utils";

const state = {
  orderList: []
};

const getters = {
  orderList: state => state.orderList.map(function (order) {
    order.createdAt = Utils.toDateTimeString(order.createdAt);
    return order;
  })
};

const mutations = {
  setOrderList(state, list) {
    state.orderList = list
  },
  deleteOrder(state, orderId) {
    state.orderList = state.orderList.filter(order => order.id !== orderId)
  }
};

const actions = {
  getOrderList({commit}) {
    let params = {
      shopId: store.getters.currentShop.id
    };
    api.get('order/undelivered', params).then(response => {
      commit("setOrderList", response.data);
    });
  },
  async completeOrder({}, order) {
    let formData = new FormData();
    formData.append("orderId", JSON.stringify(order.id));
    return await api.post('order/complete', formData);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
}
