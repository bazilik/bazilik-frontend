import store from "../../store";
import {api} from "../../main";
import Utils from "../common/utils";

const state = {
  commentList: []
};

const getters = {
  commentList: state => state.commentList.map(function (comment) {
    comment.createdAt = Utils.toDateTimeString(comment.createdAt);
    return comment;
  })
};

const mutations = {
  setCommentList(state, list) {
    state.commentList = list
  },
  deleteComment(state, commentId) {
    state.commentList = state.commentList.filter(comment => comment.id !== commentId)
  }
};

const actions = {
  getCommentList({commit}) {
    let params = {
      shopId: store.getters.currentShop.id
    };
    api.get('comment/shop', params).then(response => {
      commit("setCommentList", response.data);
    });
  },
  async deleteComment({}, commentId) {
    let formData = new FormData();
    formData.append("commentId", JSON.stringify(commentId));
    return await api.post('comment/delete', formData);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
}
