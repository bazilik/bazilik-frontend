import Vue from 'vue'
import Vuex from 'vuex'

import shop from './components/menu/shop/shop'
import category from "./components/menu/category/category"
import dish from "./components/menu/dish/dish"
import stock from "./components/stock/stock"
import order from  "./components/order/order"
import user from "./components/auth/user"
import comment from "./components/comments/comment"

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    shop,
    category,
    dish,
    stock,
    order,
    user,
    comment
  }
})
