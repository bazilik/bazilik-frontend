// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'
import router from "./router";
import store from "./store";
import App from './components/App'

import ApiClient from "./api";
import AuthManager from "./components/auth/auth"
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap-vue/dist/bootstrap-vue.min.css'
import 'bootswatch/dist/united/bootstrap.min.css'
import 'firebaseui/dist/firebaseui.css'

Vue.config.productionTip = false;
Vue.use(BootstrapVue);

export const api = new ApiClient();
export const auth = new AuthManager();
let vueApp = null;

addEventListener("onSignedIn", () => { prepareSignedInApp() }, false);
addEventListener("onSignedOut", () => { prepareSignedOutApp() }, false);

async function prepareSignedInApp() {
  store.commit("initUserData", auth.userData);
  await store.dispatch("getShopList");
  startApp()
}

function prepareSignedOutApp() {
  store.commit("resetUserData");
  startApp()
}

function startApp() {
  if (vueApp) return;

  vueApp = new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
  });
}

