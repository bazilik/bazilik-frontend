import axios from "axios"
import store from "./store"
import {auth} from "./main"

export default class ApiClient {

  // url = `https://api.buga.ga/`;
  url = `http://api.localhost/`;

  postConfig = {
    headers: {'content-type': 'undefined'}
  };

  axios = null;

  isLogging = false;

  constructor() {
    this.axios = axios.create({
      baseURL: this.url
    });

    if (!this.isLogging) return;

    this.axios.interceptors.request.use(request => {
      console.log('Starting Request', request);
      return request
    });

    this.axios.interceptors.response.use(response => {
      console.log('Response:', response);
      return response
    });
  }

  get(path, params) {
    this.addTokens();
    if (params == null) return this.axios.get(path);
    return this.axios.get(path, {params: params})
  }

  post(path, formData) {
    this.addTokens();
    return this.axios.post(path, formData, this.postConfig)
  }

  addTokens() {
    this.axios.defaults.headers.common['Api-Token'] =
      store.getters.apiToken == null ? auth.userData.apiToken : store.getters.apiToken;
    this.axios.defaults.headers.common['Firebase-Token'] =
      store.getters.firebaseToken == null ? auth.userData.firebaseToken : store.getters.firebaseToken;
  }

}
