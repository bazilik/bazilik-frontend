const express = require('express');
const connect = require('connect');
const history = require('connect-history-api-fallback');

const port = process.env.PORT || 8090;

app = connect()
  .use(history())
  .use(express.static(__dirname + "/dist"))
  .listen(port);

console.log('Express started on: http://localhost:' + port);
